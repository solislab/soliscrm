<?php
/**
 * SolisCRM Plugin
 *
 * SolisCRM is a Customer Relationship Management system for WordPress
 *
 * @package Solis\CRM
 * @subpackage Main
 */

namespace Solis\CRM\Plugin;

/**
 * Bootstrap the SolisCRM plugin
 *
 * @since  0.1
 */
function bootstrap() {
	require_once( __DIR__ . '/lib/Solis/CRM/Plugin/Autoloader.php' );
	$loader = new Autoloader();
	$loader->add_namespace( 'Solis', __DIR__ . '/lib/Solis' );
	$loader->register();
}