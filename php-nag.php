<?php
/**
 * SolisCRM Plugin
 *
 * SolisCRM is a Customer Relationship Management system for WordPress
 *
 * @package Solis\CRM
 * @subpackage Main
 */

/**
 * Add hooks to display PHP version warning and deactivate the plugin
 *
 * @access private
 * @since 0.1
 */
function crm_display_php_nag_and_deactivate() {
	add_action( 'admin_init', '_crm_action_self_deactivate' );
	add_action( 'admin_notices', '_crm_action_php_version_nag' );
	add_action( 'network_admin_notices', '_crm_action_php_version_nag' );
}

/**
 * Display warning about PHP version
 *
 * @access private
 * @since 0.1
 */
function _crm_action_php_version_nag() {
	echo '<div class="error"><p>' .
		sprintf(
			__( 'SolisCRM has deactivated itself because it requires <strong>at least PHP 5.3</strong> while your server is running PHP <strong>%s</strong>. Please contact your server administrator and request a PHP version upgrade.', 'crm' ),
			phpversion()
		) .
		'</p></div>';
}

/**
 * Self-deactivation
 *
 * @access private
 * @since  0.1
 */
function _crm_action_self_deactivate() {
	if ( ! current_user_can( 'edit_plugins' ) )
		return;

	deactivate_plugins( basename( __DIR__ ) . '/soliscrm.php' );
}