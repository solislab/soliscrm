<?php
/**
 * Solis CRM Plugin
 *
 * Solis CRM is a Customer Relationship Management system for WordPress
 *
 * @package SolisCRM
 * @subpackage Core\Entity
 */

namespace Solis\CRM\Core\Entity;

/**
 * Repository Interface
 *
 * @since 0.1
 */
interface Repository_Interface {
	/**
	 * Update an entity in the database
	 *
	 * @since  0.1
	 * @param  Entity $entity
	 * @return Repository_Interface
	 */
	public function update( Entity $entity );

	/**
	 * Create an entity in the database
	 *
	 * @since  0.1
	 * @param  Entity $entity
	 * @return Repository_Interface
	 */
	public function create( Entity $entity );

	/**
	 * Delete and entity in the database
	 *
	 * @since  0.1
	 * @param  Entity $entity
	 * @return Repository_Interface
	 */
	public function delete( Entity $entity );

	/**
	 * Find an entity by ID from the database
	 *
	 * @since  0.1
	 * @param  int $id
	 * @return Entity
	 */
	public function find( $id );

	/**
	 * Find an entity based on a property name and value
	 *
	 * @since  0.1
	 * @param  string $key
	 * @param  mixed $value
	 * @return Entity[]
	 */
	public function find_by( $key, $value );

	/**
	 * Query the database for entities matching a set of criteria
	 *
	 * @since  0.1
	 * @param  array $query_vars
	 * @return Entity[]
	 */
	public function query( $query_vars );
}