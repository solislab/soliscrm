<?php
/**
 * Solis CRM Plugin
 *
 * Solis CRM is a Customer Relationship Management system for WordPress
 *
 * @package SolisCRM
 * @subpackage Core\Entity
 */

namespace Solis\CRM\Core\Entity;

/**
 * Contact Category
 *
 * @since 0.1
 */
class Contact_Category extends Entity {
	/** ATTRIBUTES, GETTERS, SETTERS **/
	/**
	 * Name
	 *
	 * @since 0.1
	 * @var string
	 */
	private $name = '';

	/**
	 * Getter for Name
	 *
	 * @since  0.1
	 * @return string
	 */
	public function get_name() {
		return $this->name;
	}

	/**
	 * Setter for Name
	 *
	 * @since  0.1
	 * @param  string $name
	 * @return Contact_Category
	 */
	public function set_name( $name ) {
		$this->name = $name;
		return $this;
	}

	/**
	 * Description
	 *
	 * @since 0.1
	 * @var string
	 */
	private $description = '';

	/**
	 * Getter for Description
	 *
	 * @since  0.1
	 * @return string
	 */
	public function get_description() {
		return $this->description;
	}

	/**
	 * Setter for Description
	 *
	 * @since  0.1
	 * @param  string $description
	 * @return Contact_Category
	 */
	public function set_description( $description ) {
		$this->description = $description;
		return $this;
	}

	/** PUBLIC FUNCTIONS **/

	/** PROTECTED AND PRIVATE FUNCTIONS **/
}