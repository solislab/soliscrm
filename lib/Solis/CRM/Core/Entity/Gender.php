<?php
/**
 * Solis CRM Plugin
 *
 * Solis CRM is a Customer Relationship Management system for WordPress
 *
 * @package SolisCRM
 * @subpackage Core\Entity
 */

namespace Solis\CRM\Core\Entity;

/**
 * Gender Enum
 *
 * @since 0.1
 */
final class Gender {
	/** CONSTANTS **/
	const NOT_SPECIFIED = 0;
	const MALE          = 1;
	const FEMALE        = 2;

	/** PUBLIC FUNCTIONS **/

	/**
	 * Whether the supplied $gender value is valid or not
	 *
	 * @since  0.1
	 * @param  int  $gender
	 * @return boolean
	 */
	public static function is_valid( $gender ) {
		return in_array( $gender, array( self::NOT_SPECIFIED, self::MALE, self::FEMALE ) );
	}
}