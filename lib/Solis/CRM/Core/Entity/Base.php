<?php
/**
 * Solis CRM Plugin
 *
 * Solis CRM is a Customer Relationship Management system for WordPress
 *
 * @package SolisCRM
 * @subpackage Core\Entity
 */

namespace Solis\CRM\Core\Entity;

use Solis\CRM\Core\Data_Structure\Unique_Item_Interface;

/**
 * Base
 *
 * @since 0.1
 */
abstract class Base implements Unique_Item_Interface {
	/**
	 * ID
	 *
	 * @since 0.1
	 * @var int
	 */
	private $id;

	/**
	 * Getter for ID
	 *
	 * @since  0.1
	 * @return int
	 */
	public function get_id() {
		return $this->id;
	}

	/**
	 * Setter for ID
	 *
	 * @since  0.1
	 * @param  int $id
	 * @return Base
	 */
	public function set_id( $id ) {
		$this->id = (int) $id;
		return $this;
	}
}