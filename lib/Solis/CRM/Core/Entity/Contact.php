<?php
/**
 * Solis CRM Plugin
 *
 * Solis CRM is a Customer Relationship Management system for WordPress
 *
 * @package SolisCRM
 * @subpackage Core\Entity
 */

namespace Solis\CRM\Core\Entity;

use Solis\CRM\Core\Data_Structure\Address;
use Solis\CRM\Core\Data_Structure\Field_Collection;
use Solis\CRM\Core\Data_Structure\Tel;

/**
 * Contact
 *
 * @since 0.1
 */
class Contact extends Base {
	/** ATTRIBUTES, GETTERS, SETTERS **/
	private $address_collection;
	private $tel_collection;
	private $url_collection;

	/**
	 * Birthday
	 *
	 * @since 0.1
	 * @var DateTime
	 */
	private $birthday;

	/**
	 * Getter for Birthday
	 *
	 * @since  0.1
	 * @return DateTime
	 */
	public function get_birthday() {
		return $this->birthday;
	}

	/**
	 * Setter for Birthday
	 *
	 * @since  0.1
	 * @param  DateTime $birthday
	 * @return Contact
	 */
	public function set_birthday( DateTime $birthday ) {
		$this->birthday = $birthday;
		return $this;
	}

	/**
	 * Email
	 *
	 * @since 0.1
	 * @var string
	 */
	private $email = '';

	/**
	 * Getter for Email
	 *
	 * @since  0.1
	 * @return string
	 */
	public function get_email() {
		return $this->email;
	}

	/**
	 * Setter for Email
	 *
	 * @since  0.1
	 * @param  string $email
	 * @return Contact
	 */
	public function set_email( $email ) {
		$this->email = $email;
		return $this;
	}

	/**
	 * Gender
	 *
	 * @since 0.1
	 * @var int
	 */
	private $gender = Gender::NOT_SPECIFIED;

	/**
	 * Getter for Gender
	 *
	 * @since  0.1
	 * @return int
	 */
	public function get_gender() {
		return $this->gender;
	}

	/**
	 * Setter for Gender
	 *
	 * @since  0.1
	 * @param  int $gender
	 * @return Contact
	 */
	public function set_gender( $gender ) {
		if ( ! Gender::is_valid( $gender ) ) {
			throw new \InvalidArgumentException( 'Invalid gender set for ' . __CLASS__ . '::set_gender(). "' . $gender . '" was given.' );
		}

		$this->gender = $gender;
		return $this;
	}

	/**
	 * Nick Names
	 *
	 * @since 0.1
	 * @var string[]
	 */
	private $nick_names = array();

	/**
	 * Getter for Nick Names
	 *
	 * @since  0.1
	 * @return string[]
	 */
	public function get_nick_names() {
		return $this->nick_names;
	}

	/**
	 * Setter for Nick Names
	 *
	 * @since  0.1
	 * @param  string|string[] $nick_names
	 * @return Contact
	 */
	public function set_nick_names( $nick_names ) {
		$this->nick_names = (array) $nick_names;
		return $this;
	}

	/**
	 * Note
	 *
	 * @since 0.1
	 * @var string
	 */
	private $note = '';

	/**
	 * Getter for Note
	 *
	 * @since  0.1
	 * @return string
	 */
	public function get_note() {
		return $this->note;
	}

	/**
	 * Setter for Note
	 *
	 * @since  0.1
	 * @param  string $note
	 * @return Contact
	 */
	public function set_note( $note ) {
		$this->note = $note;
		return $this;
	}

	/**
	 * Organization
	 *
	 * @since 0.1
	 * @var string
	 */
	private $organization = '';

	/**
	 * Getter for Organization
	 *
	 * @since  0.1
	 * @return string
	 */
	public function get_organization() {
		return $this->organization;
	}

	/**
	 * Setter for Organization
	 *
	 * @since  0.1
	 * @param  string $organization
	 * @return Contact
	 */
	public function set_organization( $organization ) {
		$this->organization = $organization;
		return $this;
	}

	/**
	 * Role
	 *
	 * @since 0.1
	 * @var string
	 */
	private $role = '';

	/**
	 * Getter for Role
	 *
	 * @since  0.1
	 * @return string
	 */
	public function get_role() {
		return $this->role;
	}

	/**
	 * Setter for Role
	 *
	 * @since  0.1
	 * @param  string $role
	 * @return Contact
	 */
	public function set_role( $role ) {
		$this->role = $role;
		return $this;
	}

	/**
	 * Title
	 *
	 * @since 0.1
	 * @var string
	 */
	private $title = '';

	/**
	 * Getter for Title
	 *
	 * @since  0.1
	 * @return string
	 */
	public function get_title() {
		return $this->title;
	}

	/**
	 * Setter for Title
	 *
	 * @since  0.1
	 * @param  string $title
	 * @return Contact
	 */
	public function set_title( $title ) {
		$this->title = $title;
		return $this;
	}

	/**
	 * Category
	 *
	 * @since 0.1
	 * @var Contact_Category
	 */
	private $category = '';

	/**
	 * Getter for Category
	 *
	 * @since  0.1
	 * @return Contact_Category
	 */
	public function get_category() {
		return $this->category;
	}

	/**
	 * Setter for Category
	 *
	 * @since  0.1
	 * @param  Contact_Category $category
	 * @return Contact_Category
	 */
	public function set_category( $category ) {
		$this->category = $category;
		return $this;
	}

	/**
	 * Family Name
	 *
	 * @since 0.1
	 * @var string
	 */
	private $family_name = '';

	/**
	 * Getter for Family Name
	 *
	 * @since  0.1
	 * @return string
	 */
	public function get_family_name() {
		return $this->family_name;
	}

	/**
	 * Setter for Family Name
	 *
	 * @since  0.1
	 * @param  string $family_name
	 * @return Contact
	 */
	public function set_family_name( $family_name ) {
		$this->family_name = $family_name;
		return $this;
	}

	/**
	 * Given Name
	 *
	 * @since 0.1
	 * @var string
	 */
	private $given_name = '';

	/**
	 * Getter for Given Name
	 *
	 * @since  0.1
	 * @return string
	 */
	public function get_given_name() {
		return $this->given_name;
	}

	/**
	 * Setter for Given Name
	 *
	 * @since  0.1
	 * @param  string $given_name
	 * @return Contact
	 */
	public function set_given_name( $given_name ) {
		$this->given_name = $given_name;
		return $this;
	}

	/**
	 * Additional Names
	 *
	 * @since 0.1
	 * @var string[]
	 */
	private $additional_names = array();

	/**
	 * Getter for Additional Names
	 *
	 * @since  0.1
	 * @return string[]
	 */
	public function get_additional_names() {
		return $this->additional_names;
	}

	/**
	 * Setter for Additional Names
	 *
	 * @since  0.1
	 * @param  string[] $additional_names
	 * @return Contact
	 */
	public function set_additional_names( $additional_names ) {
		$this->additional_names = (array) $additional_names;
		return $this;
	}

	/**
	 * Prefixes
	 *
	 * @since 0.1
	 * @var string[]
	 */
	private $prefixes = array();

	/**
	 * Getter for Prefixes
	 *
	 * @since  0.1
	 * @return string[]
	 */
	public function get_prefixes() {
		return $this->prefixes;
	}

	/**
	 * Setter for Prefixes
	 *
	 * @since  0.1
	 * @param  string[] $prefixes
	 * @return Contact
	 */
	public function set_prefixes( $prefixes ) {
		$this->prefixes = (array) $prefixes;
		return $this;
	}

	/**
	 * Suffixes
	 *
	 * @since 0.1
	 * @var string[]
	 */
	private $suffixes = array();

	/**
	 * Getter for Suffixes
	 *
	 * @since  0.1
	 * @return string[]
	 */
	public function get_suffixes() {
		return $this->suffixes;
	}

	/**
	 * Setter for Suffixes
	 *
	 * @since  0.1
	 * @param  string[] $suffixes
	 * @return Contact
	 */
	public function set_suffixes( $suffixes ) {
		$this->suffixes = (array) $suffixes;
		return $this;
	}

	/** PUBLIC FUNCTIONS **/

	/**
	 * Constructor
	 */
	public function __construct() {
		$this->address_collection = new Field_Collection;
		$this->tel_collection = new Field_Collection;
		$this->url_collection = new Field_Collection;
	}

	/**
	 * Add an address to this contact.
	 *
	 * @since 0.1
	 * @param Address $address
	 * @return Contact
	 */
	public function add_address( Address $address ) {
		$this->address_collection->add( $address );

		return $this;
	}

	/**
	 * Get all addresses for this contact
	 *
	 * @since 0.1
	 * @return Address[]
	 */
	public function get_addresses() {
		return $this->address_collection->get_items();
	}

	/**
	 * Get all addresses that have certain types.
	 *
	 * @since  0.1
	 * @param  string|string[] $types
	 * @return Address[]
	 */
	public function get_address_by_type( $types ) {
		return $this->address_collection->find_by_type( (array) $types );
	}

	/**
	 * Add a phone number to this contact
	 *
	 * @since 0.1
	 * @param Tel $tel
	 * @return Contact
	 */
	public function add_tel( Tel $tel ) {
		$this->tel_collection->add( $tel );

		return $this;
	}

	/**
	 * Get all phone numbers belonging to this contact
	 *
	 * @since 0.1
	 * @return Tel[]
	 */
	public function get_tels() {
		return $this->tel_collection->get_items();
	}

	/**
	 * Get all phone numbers that have certain types
	 *
	 * @since 0.1
	 * @param  string|string[] $types
	 * @return Tel[]
	 */
	public function get_tel_by_type( $types ) {
		return $this->tel_collection->find_by_type( (array) $types );
	}

	/**
	 * Add an URL to this contact
	 *
	 * @since 0.1
	 * @param String_Field $url
	 * @return Contact
	 */
	public function add_url( String_Field $url ) {
		$this->url_collection->add( $url );

		return $this;
	}

	/**
	 * Get all URLs belonging to this contact
	 *
	 * @since  0.1
	 * @return String_Field[]
	 */
	public function get_urls() {
		return $this->url_collection->get_items();
	}

	/**
	 * Get all URLs belonging to certain types
	 *
	 * @since  0.1
	 * @param  string|string[] $types
	 * @return String_Field[]
	 */
	public function get_url_by_type( $types ) {
		return $this->url_collection->find_by_type( (array) $types );
	}

	/**
	 * Supports deep cloning
	 *
	 * @since 0.1
	 * @return void
	 */
	public function __clone() {
		$this->address_collection = clone $this->address_collection;
		$this->tel_collection     = clone $this->tel_collection;
		$this->url_collection     = clone $this->url_collection;
	}

	/** PROTECTED AND PRIVATE FUNCTIONS **/
}