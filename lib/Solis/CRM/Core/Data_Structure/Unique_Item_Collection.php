<?php
/**
 * Solis CRM Plugin
 *
 * Solis CRM is a Customer Relationship Management system for WordPress
 *
 * @package SolisCRM
 * @subpackage Core\Data_Structure
 */

namespace Solis\CRM\Core\Data_Structure;

/**
 * Collection
 *
 * @since 0.1
 */
class Unique_Item_Collection implements \Countable, \IteratorAggregate {
	/** ATTRIBUTES, GETTERS, SETTERS **/

	/**
	 * Hold all the items.
	 *
	 * @since 0.1
	 * @var stdObject[]
	 */
	protected $items = array();

	/** PUBLIC FUNCTIONS **/

	/**
	 * Add an item to the collection
	 *
	 * @since 0.1
	 * @param Unique_Item_Interface $item
	 */
	public function add( Unique_Item_Interface $item ) {
		$this->items[ $item->get_id() ] = $item;

		return $this;
	}

	/**
	 * Find an item by its ID
	 *
	 * @since 0.1
	 * @param  string|int $id
	 * @return Unique_Item_Interface
	 */
	public function find( $id ) {
		if ( ! isset( $this->items[ $id ] ) )
			return null;

		return $this->items[ $id ];
	}

	/**
	 * Get all items in the collection
	 *
	 * @since  0.1
	 * @return Unique_Item_Interface[]
	 */
	public function get_items() {
		return array_values( $this->items );
	}

	/**
	 * Remove an item from the collection
	 *
	 * @since  0.1
	 * @param  Unique_Item_Interface $item
	 * @return Collection
	 */
	public function remove( Unique_Item_Interface $item ) {
		if ( isset( $this->items[ $item->get_id() ] ) )
			unset( $this->items[ $item->get_id() ] );

		return $this;
	}

	/**
	 * Return the number of items in this collection.
	 *
	 * @return int
	 */
	public function count() {
		return count( $this->items );
	}

	/**
	 * Implements IteratorAggregator::getIterator.
	 *
	 * This allows iterators to be used with the collection.
	 *
	 * @since  0.1
	 * @return ArrayIterator
	 */
	public function getIterator() {
		return new ArrayIterator( array_values( $this->items ) );
	}
	/** PROTECTED AND PRIVATE FUNCTIONS **/
}