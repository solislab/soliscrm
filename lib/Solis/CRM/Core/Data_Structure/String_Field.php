<?php
/**
 * Solis CRM Plugin
 *
 * Solis CRM is a Customer Relationship Management system for WordPress
 *
 * @package SolisCRM
 * @subpackage Core\Data_Structure
 */

namespace Solis\CRM\Core\Data_Structure;

/**
 * String Field
 *
 * @since 0.1
 */
class String_Field extends Field {
	/** ATTRIBUTES, GETTERS, SETTERS **/
	/**
	 * Value
	 *
	 * @since 0.1
	 * @var string
	 */
	private $value = '';

	/**
	 * Getter for Value
	 *
	 * @since  0.1
	 * @return string
	 */
	public function get_value() {
		return $this->value;
	}

	/**
	 * Setter for Value
	 *
	 * @since  0.1
	 * @param  string $value
	 * @return String_Field
	 */
	public function set_value( $value ) {
		if ( ! is_string( $value ) )
			throw new \InvalidArgumentException( __CLASS__ . '::set_value() only accepts string values. Input was of type "' . gettype( $value ) . '".' );

		$this->value = $value;
		return $this;
	}

	public function __construct( $value ) {
		parent::__construct();
		$this->value = $value;
	}
}