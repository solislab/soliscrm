<?php
/**
 * Solis CRM Plugin
 *
 * Solis CRM is a Customer Relationship Management system for WordPress
 *
 * @package SolisCRM
 * @subpackage Core\Data_Structure
 */

namespace Solis\CRM\Core\Data_Structure;

/**
 * Type Interface
 *
 * @since 0.1
 */
interface Type_Item_Interface {
	/** ATTRIBUTES, GETTERS, SETTERS **/
	/**
	 * Get types assigned to this object
	 *
	 * @since 0.1
	 * @return string[]
	 */
	public function get_types();

	/**
	 * Assign types to this object
	 *
	 * @since 0.1
	 * @param string|string[] $types
	 */
	public function set_types( $types );

	/** PUBLIC FUNCTIONS **/
	/**
	 * Whether this object have all the specified types
	 *
	 * @since  0.1
	 * @param  string|string[] $types
	 * @return bool
	 */
	public function belongs_to_types( $types );
}