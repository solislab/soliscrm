<?php
/**
 * Solis CRM Plugin
 *
 * Solis CRM is a Customer Relationship Management system for WordPress
 *
 * @package SolisCRM
 * @subpackage Core\Data_Structure
 */

namespace Solis\CRM\Core\Data_Structure;

/**
 * Data Item Interface
 *
 * @since 0.1
 */
interface Unique_Item_Interface {

	/**
	 * Get the unique ID of this data item
	 * @return int|string
	 */
	public function get_id();
}