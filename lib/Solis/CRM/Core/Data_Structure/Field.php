<?php
/**
 * Solis CRM Plugin
 *
 * Solis CRM is a Customer Relationship Management system for WordPress
 *
 * @package SolisCRM
 * @subpackage Core\Data_Structure
 */

namespace Solis\CRM\Core\Data_Structure;

/**
 * Field class
 *
 * @since 0.1
 */
class Field implements Field_Interface {
	/** ATTRIBUTES, GETTERS, SETTERS **/

	/**
	 * Unique ID
	 *
	 * @since 0.1
	 * @var string
	 */
	private $id = '';

	/**
	 * Getter for Unique ID
	 *
	 * @since  0.1
	 * @return string
	 */
	public function get_id() {
		return $this->id;
	}

	/**
	 * Field Types
	 *
	 * @since 0.1
	 * @var string[]
	 */
	private $types = array();

	/**
	 * Getter for Field Type
	 *
	 * @since  0.1
	 * @return string[]
	 */
	public function get_types() {
		return $this->types;
	}

	/**
	 * Setter for Field Type
	 *
	 * @since  0.1
	 * @param  string|string[] $types
	 * @return Field
	 */
	public function set_types( $types ) {
		$this->types = (array) $types;
		return $this;
	}

	/** PUBLIC FUNCTIONS **/

	public function __construct() {
		// Generate an internal random unique ID for this object
		$this->id = uniqid( '', true );
	}

	public function belongs_to_types( $types ) {;
		$types = (array) $types;
		return array_intersect( $types, $this->types ) == $types;
	}

	/** PRIVATE FUNCTIONS **/
}