<?php
/**
 * Solis CRM Plugin
 *
 * Solis CRM is a Customer Relationship Management system for WordPress
 *
 * @package SolisCRM
 * @subpackage Core\Data_Structure
 */

namespace Solis\CRM\Core\Data_Structure;

/**
 * Address
 *
 * @since 0.1
 */
class Address extends Field {
	/** ATTRIBUTES, GETTERS, SETTERS **/

	/**
	 * PO Box
	 *
	 * @since 0.1
	 * @var string
	 */
	private $po_box = '';

	/**
	 * Getter for PO Box
	 *
	 * @since  0.1
	 * @return string
	 */
	public function get_po_box() {
		return $this->po_box;
	}

	/**
	 * Setter for PO Box
	 *
	 * @since  0.1
	 * @param string $po_box
	 * @return Address
	 */
	public function set_po_box( $po_box ) {
		$this->po_box = $po_box;
		return $this;
	}

	/**
	 * Extended Address
	 *
	 * This is usually apartment or suite number.
	 *
	 * @since 0.1
	 * @var string
	 */
	private $ext = '';

	/**
	 * Getter for Extended Address
	 *
	 * @since  0.1
	 * @return string
	 */
	public function get_ext() {
		return $this->ext;
	}

	/**
	 * Setter for Extended Address
	 *
	 * @since  0.1
	 * @param string $ext
	 * @return Address
	 */
	public function set_ext( $ext ) {
		$this->ext = $ext;
		return $this;
	}

	/**
	 * Street Address
	 *
	 * E.g. 1 Infinite Loop
	 *
	 * @since 0.1
	 * @var string
	 */
	private $street = '';

	/**
	 * Getter for Street Address
	 *
	 * @since  0.1
	 * @return string
	 */
	public function get_street() {
		return $this->street;
	}

	/**
	 * Setter for Street Address
	 *
	 * @since  0.1
	 * @param string $street
	 * @return Address
	 */
	public function set_street( $street ) {
		$this->street = $street;
		return $this;
	}

	/**
	 * City
	 *
	 *	E.g. San Francisco
	 *
	 * @since 0.1
	 * @var string
	 */
	private $city = '';

	/**
	 * Getter for City
	 *
	 * @since  0.1
	 * @return string
	 */
	public function get_city() {
		return $this->city;
	}

	/**
	 * Setter for City
	 *
	 * @since  0.1
	 * @param string $city
	 * @return Address
	 */
	public function set_city( $city ) {
		$this->city = $city;
		return $this;
	}

	/**
	 * State
	 *
	 * E.g. California
	 *
	 * @since 0.1
	 * @var string
	 */
	private $state = '';

	/**
	 * Getter for State
	 *
	 * @since  0.1
	 * @return string
	 */
	public function get_state() {
		return $this->state;
	}

	/**
	 * Setter for State
	 *
	 * @since  0.1
	 * @param string $state
	 * @return Address
	 */
	public function set_state( $state ) {
		$this->state = $state;
		return $this;
	}

	/**
	 * Postal Code
	 *
	 * @since 0.1
	 * @var string
	 */
	private $postal_code = '';

	/**
	 * Getter for Postal Code
	 *
	 * @since  0.1
	 * @return string
	 */
	public function get_postal_code() {
		return $this->postal_code;
	}

	/**
	 * Setter for Postal Code
	 *
	 * @since  0.1
	 * @param string $postal_code
	 * @return Address
	 */
	public function set_postal_code( $postal_code ) {
		$this->postal_code = preg_replace( '/[^\d]/', '', $postal_code );
		return $this;
	}

	/**
	 * Country Code
	 *
	 * ISO 3611 alpha-2 country code (e.g. US)
	 *
	 * @since 0.1
	 * @var string
	 */
	private $country_code = '';

	/**
	 * Getter for Country Code
	 *
	 * @since  0.1
	 * @return string
	 */
	public function get_country_code() {
		return $this->country_code;
	}

	/**
	 * Setter for Country Code
	 *
	 * @since  0.1
	 * @param string $country_code
	 * @return Address
	 */
	public function set_country_code( $country_code ) {
		// This is a very simple validation of country code, only checks the length.
		// Country codes do change over time so it can be a bit complicated
		// trying to accomodate legacy country codes in existing database,
		// so this simple check will do for now.
		if ( strlen( $country_code ) !== 2 ) {
			throw new \InvalidArgumentException( __CLASS__ . '::set_country_code() only accepts ISO 3611 alpha-2 countrycode. Input was "' . $country_code . '".' );
		}

		$this->country_code = $country_code;
		return $this;
	}

	/** PUBLIC FUNCTIONS **/

	/** PROTECTED AND PRIVATE FUNCTIONS **/
}