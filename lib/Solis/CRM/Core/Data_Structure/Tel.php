<?php
/**
 * Solis CRM Plugin
 *
 * Solis CRM is a Customer Relationship Management system for WordPress
 *
 * @package SolisCRM
 * @subpackage Core\Data_Structure
 */

namespace Solis\CRM\Core\Data_Structure;

/**
 * Phone Number
 *
 * @since 0.1
 */
class Tel extends Field {
	/** ATTRIBUTES, GETTERS, SETTERS **/

	/**
	 * Extension
	 *
	 * @since 0.1
	 * @var string
	 */
	private $ext = '';

	/**
	 * Getter for Extension
	 *
	 * @since  0.1
	 * @return string
	 */
	public function get_ext() {
		return $this->ext;
	}

	/**
	 * Setter for Extension
	 *
	 * @since  0.1
	 * @param  string $ext
	 * @return Tel
	 */
	public function set_ext( $ext ) {
		$this->ext = $ext;
		return $this;
	}

	/**
	 * National Number
	 *
	 * @since 0.1
	 * @var string
	 */
	private $national_number = '';

	/**
	 * Getter for National Number.
	 *
	 * @since  0.1
	 * @return string
	 */
	public function get_national_number() {
		return $this->national_number;
	}

	/**
	 * Setter for National Number
	 *
	 * @since  0.1
	 * @param  string $national_number
	 * @return Tel
	 */
	public function set_national_number( $national_number ) {
		$this->national_number = preg_replace( '/[^\d]/', '', $national_number );
		return $this;
	}

	/**
	 * Global Code
	 *
	 * @since 0.1
	 * @var string
	 */
	private $global_code = '';

	/**
	 * Getter for Global Code
	 *
	 * @since  0.1
	 * @return string
	 */
	public function get_global_code() {
		return $this->global_code;
	}

	/**
	 * Setter for Global Code
	 *
	 * @since  0.1
	 * @param  string $global_code
	 * @return Tel
	 */
	public function set_global_code( $global_code ) {
		$this->global_code = preg_replace( '/[^\d]/', '', $global_code );
		return $this;
	}

	/** PUBLIC FUNCTIONS **/

	/** PROTECTED AND PRIVATE FUNCTIONS **/
}