<?php
/**
 * Solis CRM Plugin
 *
 * Solis CRM is a Customer Relationship Management system for WordPress
 *
 * @package SolisCRM
 * @subpackage Core\Data_Structure
 */

namespace Solis\CRM\Core\Data_Structure;

/**
 * Unique Type Item Interface
 *
 * @since 0.1
 */
interface Field_Interface extends Unique_Item_Interface, Type_Item_Interface {
}