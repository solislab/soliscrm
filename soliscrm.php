<?php
/**
 * Solis CRM Plugin
 *
 * Solis CRM is a Customer Relationship Management system for WordPress
 *
 * @package SolisCRM
 * @subpackage Main
 */

/*
Plugin Name: SolisCRM
Version: 0.1-alpha
Description: Customer Relationship Management for WordPress
Author: SolisLab
Author URI: http://solislab.com
Plugin URI: http://solislab.com/soliscrm/
Text Domain: crm
Domain Path: /languages
*/

// Load l10n textdomain
require 'textdomain.php';
crm_load_textdomain();

// Deactivate and display notice if running PHP 5.2 or below
if ( version_compare( phpversion(), '5.3', '<' ) ) {
	require 'php-nag.php';
	crm_display_php_nag_and_deactivate();
	return;
}

// Initialize the plugin
require 'bootstrap.php';

// use call_user_func to avoid fatal parse error in PHP 5.2 where namespace
// is not supported
call_user_func( 'Solis\CRM\Plugin\bootstrap' );