<?php
/**
 * SolisCRM Plugin
 *
 * SolisCRM is a Customer Relationship Management system for WordPress
 *
 * @package Solis\CRM
 * @subpackage Main
 */

/**
 * Setup textdomain for l10n
 *
 * @since 0.1
 */
function crm_load_textdomain() {
	add_action( 'plugins_loaded', '_crm_action_load_textdomain' );
}

/**
 * Load plugin textdomain from `languages` subfolder
 *
 * @since 0.1
 * @access private
 */
function _crm_action_load_textdomain() {
	load_plugin_textdomain( 'crm', false, basename( __DIR__ ) . '/languages' );
}